# include <iostream>
# include <string.h>
# include <libpq-fe.h>
# include "iomanip"
int main()
{
  PGresult *res;
  ExecStatusType status_return;
  PGPing code_retour_ping;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port = 5432");

  if(code_retour_ping == PQPING_OK)
  {
    std::cout << "La connexion au serveur de base de données à été établie avec succès" << std::endl;
    PGconn *connexion;
    connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 user=a.siry dbname=a.siry");

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      char *host = PQhost(connexion);
      char *user = PQuser(connexion);
      char *pass = PQpass(connexion);
      char *bdd = PQdb(connexion);
      char *porttcp = PQport(connexion);
      int ssl = PQsslInUse(connexion);
      const char *param = "server_encoding";
      const char *encodage = PQparameterStatus(connexion, param);
      int bibliolibpq = PQlibVersion();
      int versionP = PQprotocolVersion(connexion);
      int versionS = PQserverVersion(connexion);
      std::cout << "La connexion au serveur" << host << "a été établie avec les paramètres suivants :" << std::endl;
      std::cout << "Utilisateur : " << user << std::endl;
      std::cout << "Mot de passe : ";

      for(int a = 0; a < strlen(pass); ++a)
      {
        std::cout << "*";
      }

      std::cout << std::endl;
      std::cout << "\t * base de données : " << bdd << std::endl;
      std::cout << "\t * port TCP : " << porttcp << std::endl;
      std::cout << "\t * chiffrement ssl : " << (PQsslInUse(connexion) ? "true" : "false") << std::endl;
      std::cout << "\t * encodage : " << encodage << std::endl;
      std::cout << "\t * version du protocole : " << versionP << std::endl;
      std::cout << "\t * version du serveur : " << versionS << std::endl;
      std::cout << "\t * version de la bibliothèque 'libpq' du client : " << bibliolibpq << std::endl;
    }

    res = PQexec(connexion, "SELECT \"Animal\".\"id\",\"Animal\".\"nom\", \"Animal\".\"sexe\", \"Animal\".\"date_naissance\", \"Animal\".\"commentaires\", \"Race\".\"nom\", \"Rare\".\"description\" FROM \"si6\".\"Race\" INNER JOIN \"Animal\" ON \"Race\".\"id\" = \"Animal\".\"race_id\" WHERE \"Race\".\"nom\" = 'Singapura' and \"sexe\" = 'Femelle'");
    status_return = PQresultStatus(res);
    if (status_return == PGRES_TUPLES_OK)
    {
      std::cout<<"Fin avec succès d'une commande renvoyant des données"<<std::endl;
      int ligne = PQntuples(res);
      int colonne = PQnfields(res);
      const char *separation ="|"; // Séparation des champs
      int compteur=0;
      int max=0;
      for (int i=0; i<colonne; i++)
      {
        char *champs = PQfname(res, i);

        for(int l = 0; champs[l] != '\0'; l++)
        { {
            compteur++;
          }

          if(compteur > max)
          {
            max = compteur;
          }

          compteur = 0;
        }

        for (int h = 0; h < colonne; h++)
        {
          char *entete=PQfname(res, h);
          std::cout<<separation<<std::setw(max)<<std::left<<entete;
        }
        std::cout<<std::endl;
        for (int i = 0; i < ligne; i++)
        {
          for (int f = 0 ; f < colonne; f++)
          {
            std::cout<<separation<<std::setw(max)<<std::left<<PQgetvalue(res, i, f);
          }
          std::cout <<std::endl;
        }
        std::cout<<"L'exécution de la requête SQL àa retourné "<< ligne <<" enregistrements"<<std::endl;
      }   }
      else if(status_return == PGRES_EMPTY_QUERY)
      {
        std::cout << "La chaîne envoyée au serveur était vide." << std::endl;
      }
      else if(status_return == PGRES_COMMAND_OK)
      {
        std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
      }
      else if(status_return == PGRES_COPY_OUT)
      {
        std::cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
      }
      else if(status_return == PGRES_COPY_IN)
      {
        std::cout << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
      }
      else if(status_return == PGRES_BAD_RESPONSE)
      {
        std::cout << "La réponse du serveur n'a pas été comprise." << std::endl;
      }
      else if(status_return == PGRES_NONFATAL_ERROR)
      {
        std::cout << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
      }
      else if(status_return == PGRES_FATAL_ERROR)
      {
        std::cout << "Une erreur fatale est survenue." << std::endl;
      }
      else if(status_return == PGRES_COPY_BOTH)
      {
        std::cout << "" << std::endl;
      }
      else if(status_return == PGRES_SINGLE_TUPLE)
      {
        std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode simple ligne a été sélectionné pour cette requête" << std::endl;
      }

    else
    {
      std::cerr<<"Malheursement le serveur n'est pas joignable"
               << std::endl;
    }
     return 0;
  }
}
